{
  "openapi" : "3.0.0",
  "servers" : [ {
    "url" : "https://virtserver.swaggerhub.com/Port-of-Rotterdam/Navigate/0.0.1"
  }, {
    "url" : "http://virtserver.swaggerhub.com/Port-of-Rotterdam/Navigate/0.0.1"
  } ],
  "info" : {
    "description" : "API for navigate\n",
    "version" : "0.0.1-oas3",
    "title" : "Navigate API",
    "termsOfService" : "http://swagger.io/terms/",
    "contact" : {
      "email" : "jeroen.drenth@clockwork.nl"
    }
  },
  "paths" : {
    "/connections/{origin}/{destination}" : {
      "get" : {
        "tags" : [ "Connections" ],
        "summary" : "Find connections",
        "description" : "Find connections between two places",
        "operationId" : "getConnections",
        "parameters" : [ {
          "name" : "origin",
          "in" : "path",
          "description" : "The origin of the connection. Formatted as {city},{country}.",
          "example" : "Shanghai,Netherlands",
          "required" : true,
          "schema" : {
            "type" : "string"
          }
        }, {
          "name" : "destination",
          "in" : "path",
          "description" : "The origin of the connection. Formatted as {city},{country}.",
          "example" : "Rotterdam,Netherlands",
          "required" : true,
          "schema" : {
            "type" : "string"
          }
        }, {
          "name" : "via",
          "in" : "query",
          "description" : "An array of places to be included in the connection. Each value must be formatted as {city},{country}.",
          "required" : false,
          "style" : "pipeDelimited",
          "explode" : false,
          "schema" : {
            "type" : "array",
            "items" : {
              "type" : "string",
              "pattern" : "/^(?:\\w+,){2,}(?:\\w+)$/"
            }
          },
          "example" : [ "Amsterdam,Netherlands", "Colombo,Sri Lanka" ]
        }, {
          "name" : "departureTime",
          "in" : "query",
          "description" : "Time of depature. Formatted as a unix timestamp.",
          "required" : false,
          "schema" : {
            "type" : "string",
            "format" : "dateTime"
          },
          "example" : "1511347086"
        }, {
          "name" : "arrivalTime",
          "in" : "query",
          "description" : "Time of arrival. Formatted as a unix timestamp.",
          "required" : false,
          "schema" : {
            "type" : "string",
            "format" : "dateTime"
          },
          "example" : "1511347086"
        }, {
          "name" : "modalityType",
          "in" : "query",
          "description" : "Filters on modality type.",
          "required" : false,
          "schema" : {
            "$ref" : "#/components/schemas/ModalityType"
          },
          "example" : "deepsea"
        }, {
          "name" : "sort",
          "in" : "query",
          "style" : "pipeDelimited",
          "explode" : false,
          "schema" : {
            "type" : "array",
            "items" : {
              "type" : "string",
              "pattern" : "[-+](cost|emissionsInKilograms|etd|legCount|travelTimeInMinutes)"
            },
            "default" : [ "-travelTimeInMinutes" ]
          },
          "description" : "<p>The sort parameters are in a pipe separated array named sort, each of them starting with + for an ascending sort or - for a descending one.</p> <p>Available attributes are</p> <ul>\n  <li>travelTimeInMinutes</li>\n  <li>etd</li>\n  <li>cost</li>\n  <li>legCount</li>\n  <li>emissionsInKilograms</li>\n</ul>",
          "example" : [ "-travelTimeInMinutes", "+etd", "+emissionsInKilograms" ]
        } ],
        "responses" : {
          "200" : {
            "description" : "successful operation",
            "content" : {
              "application/json" : {
                "schema" : {
                  "type" : "object",
                  "$ref" : "#/components/schemas/Connections"
                }
              }
            }
          },
          "400" : {
            "description" : "Invalid parameter"
          },
          "404" : {
            "description" : "No connection found"
          }
        }
      }
    },
    "/companies" : {
      "get" : {
        "tags" : [ "Companies" ],
        "summary" : "Get companies.",
        "description" : "Get companies.",
        "parameters" : [ {
          "name" : "types",
          "in" : "query",
          "description" : "The types of companies to search for.",
          "required" : true,
          "style" : "spaceDelimited",
          "explode" : false,
          "schema" : {
            "type" : "array",
            "items" : {
              "$ref" : "#/components/schemas/CompanyType"
            }
          },
          "example" : [ "company", "operator" ]
        }, {
          "name" : "near",
          "in" : "query",
          "description" : "A Google place ID of a location the terminal is near to.",
          "schema" : {
            "type" : "string"
          },
          "example" : "ChIJfcRUX2C3xUcRhUtelay7KVI"
        }, {
          "name" : "companyServices",
          "in" : "query",
          "description" : "A space delimited array of ids of services a company should have. See <a href=\"#model-CompanyType\">CompanyService</a><span class=\"model-title\">",
          "style" : "spaceDelimited",
          "explode" : false,
          "schema" : {
            "type" : "array",
            "items" : {
              "type" : "string"
            }
          },
          "example" : [ 82, 87 ]
        }, {
          "name" : "terminalServices",
          "in" : "query",
          "description" : "A space delimited array of ids of services a terminal should have. See <a href=\"#model-TerminalType\">TerminalService</a>",
          "style" : "spaceDelimited",
          "explode" : false,
          "schema" : {
            "type" : "array",
            "items" : {
              "type" : "string"
            }
          },
          "example" : [ 82, 87 ]
        }, {
          "name" : "operators",
          "in" : "query",
          "description" : "The container owner of an empty depot at a terminal. Since the container owner is an operator, this value should be an operator id. This parameters is only applied when terminal is one of the types.",
          "schema" : {
            "type" : "string"
          },
          "example" : 2004
        }, {
          "name" : "dropoff",
          "in" : "query",
          "description" : "Indicates whether the terminal has dropoff services. This parameters is only applied when terminal is one of the types.",
          "schema" : {
            "type" : "boolean"
          },
          "required" : false,
          "example" : true
        }, {
          "name" : "pickup",
          "in" : "query",
          "description" : "Indicates whether the terminal has pickup services. This parameters is only applied when terminal is one of the types.",
          "schema" : {
            "type" : "boolean"
          },
          "required" : false,
          "example" : true
        } ],
        "responses" : {
          "200" : {
            "description" : "successful operation. Return an operator, terminal or other.",
            "content" : {
              "application/json" : {
                "schema" : {
                  "type" : "array",
                  "description" : "An array of company. A company can by an operator, terminal or other.",
                  "items" : {
                    "oneOf" : [ {
                      "$ref" : "#/components/schemas/Other"
                    }, {
                      "$ref" : "#/components/schemas/Operator"
                    }, {
                      "$ref" : "#/components/schemas/Terminal"
                    } ],
                    "example" : {
                      "$ref" : "#/components/schemas/Other"
                    }
                  }
                }
              }
            }
          },
          "400" : {
            "description" : "Invalid parameter"
          },
          "404" : {
            "description" : "No connection found"
          }
        }
      }
    },
    "/companies/{id}" : {
      "get" : {
        "tags" : [ "Companies" ],
        "summary" : "Get companies by id.",
        "description" : "Get companies by id.",
        "parameters" : [ {
          "name" : "id",
          "in" : "path",
          "description" : "The id of the company.",
          "example" : 1111181,
          "required" : true,
          "schema" : {
            "type" : "integer"
          }
        } ],
        "responses" : {
          "200" : {
            "description" : "successful operation. Return a company, operator or terminal.",
            "content" : {
              "application/json" : {
                "schema" : {
                  "oneOf" : [ {
                    "$ref" : "#/components/schemas/Other"
                  }, {
                    "$ref" : "#/components/schemas/Operator"
                  }, {
                    "$ref" : "#/components/schemas/Terminal"
                  } ],
                  "example" : {
                    "$ref" : "#/components/schemas/Operator"
                  }
                }
              }
            }
          },
          "400" : {
            "description" : "Invalid parameter {parameter}"
          },
          "404" : {
            "description" : "No connection found"
          }
        }
      }
    }
  },
  "externalDocs" : {
    "description" : "Find out more about Swagger",
    "url" : "http://swagger.io"
  },
  "components" : {
    "schemas" : {
      "Connections" : {
        "type" : "object",
        "properties" : {
          "data" : {
            "type" : "array",
            "items" : {
              "$ref" : "#/components/schemas/Connection"
            }
          }
        }
      },
      "Connection" : {
        "type" : "object",
        "required" : [ "ConnectionId" ],
        "properties" : {
          "ConnectionId" : {
            "type" : "integer"
          },
          "origin" : {
            "$ref" : "#/components/schemas/Place"
          },
          "destination" : {
            "$ref" : "#/components/schemas/Place"
          },
          "travelTimeInMinutes" : {
            "type" : "integer"
          },
          "distanceInKilometers" : {
            "type" : "integer"
          },
          "etd" : {
            "type" : "integer",
            "description" : "Expected time of departure. Formatted as a unix timestamp.",
            "example" : 1511347086
          },
          "eta" : {
            "type" : "integer",
            "description" : "Expected time of arrival. Formatted as a unix timestamp.",
            "example" : 1511347086
          },
          "legs" : {
            "type" : "array",
            "items" : {
              "$ref" : "#/components/schemas/Leg"
            }
          },
          "polyline" : {
            "type" : "string",
            "format" : "base64"
          }
        }
      },
      "Leg" : {
        "type" : "object",
        "properties" : {
          "origin" : {
            "$ref" : "#/components/schemas/Stop"
          },
          "destination" : {
            "$ref" : "#/components/schemas/Stop"
          },
          "modalityType" : {
            "$ref" : "#/components/schemas/ModalityType"
          },
          "distanceInKilometers" : {
            "type" : "string",
            "description" : "The distance in kilometers of this leg of the connection."
          },
          "emissionsInKilograms" : {
            "type" : "string",
            "description" : "Emissions (in kg CO2) for this leg."
          },
          "operators" : {
            "type" : "array",
            "items" : {
              "$ref" : "#/components/schemas/Operator"
            }
          }
        }
      },
      "Other" : {
        "type" : "object",
        "properties" : {
          "id" : {
            "type" : "string",
            "description" : "Id of the entity."
          },
          "name" : {
            "type" : "string",
            "description" : "Name of the terminal."
          },
          "location" : {
            "$ref" : "#/components/schemas/Coordinates"
          },
          "address" : {
            "$ref" : "#/components/schemas/Address"
          },
          "website" : {
            "type" : "string",
            "description" : "The url of the company's website."
          },
          "email" : {
            "type" : "string",
            "description" : "The email address of the company."
          },
          "phone" : {
            "type" : "string",
            "description" : "The main phone number of the company."
          },
          "branch" : {
            "type" : "string",
            "description" : "The type of branch of the company."
          },
          "companyServices" : {
            "type" : "array",
            "items" : {
              "$ref" : "#/components/schemas/CompanyService"
            }
          }
        }
      },
      "Terminal" : {
        "allOf" : [ {
          "$ref" : "#/components/schemas/Other"
        }, {
          "type" : "object",
          "properties" : {
            "terminalServices" : {
              "type" : "array",
              "items" : {
                "$ref" : "#/components/schemas/TerminalService"
              }
            }
          }
        } ]
      },
      "Operator" : {
        "allOf" : [ {
          "$ref" : "#/components/schemas/Other"
        }, {
          "type" : "object",
          "properties" : { }
        } ]
      },
      "Place" : {
        "type" : "object",
        "properties" : {
          "name" : {
            "type" : "string"
          },
          "location" : {
            "$ref" : "#/components/schemas/Coordinates"
          }
        }
      },
      "Coordinates" : {
        "type" : "object",
        "properties" : {
          "longitude" : {
            "type" : "number"
          },
          "latitude" : {
            "type" : "number"
          }
        }
      },
      "Stop" : {
        "type" : "object",
        "properties" : {
          "name" : {
            "type" : "string"
          },
          "location" : {
            "$ref" : "#/components/schemas/Coordinates"
          },
          "address" : {
            "$ref" : "#/components/schemas/Address"
          }
        }
      },
      "Address" : {
        "type" : "object",
        "properties" : {
          "street" : {
            "type" : "string"
          },
          "number" : {
            "type" : "string"
          },
          "postfix" : {
            "type" : "string"
          },
          "postalCode" : {
            "type" : "string"
          },
          "city" : {
            "type" : "string"
          },
          "country" : {
            "type" : "string"
          },
          "countryCode" : {
            "type" : "string"
          }
        }
      },
      "TerminalService" : {
        "type" : "object",
        "properties" : {
          "id" : {
            "type" : "string",
            "description" : "The id of the terminal service",
            "example" : 87
          },
          "name" : {
            "type" : "string",
            "description" : "The name of the terminal service",
            "example" : "Reefer connections"
          }
        }
      },
      "CompanyService" : {
        "type" : "object",
        "properties" : {
          "id" : {
            "type" : "string",
            "description" : "The id of the company service",
            "example" : 36
          },
          "name" : {
            "type" : "string",
            "description" : "The name of the company service",
            "example" : "Average agents and adjusters"
          }
        }
      },
      "CompanyType" : {
        "type" : "string",
        "enum" : [ "operator", "other", "terminal" ]
      },
      "ModalityType" : {
        "type" : "string",
        "enum" : [ "barge", "deepsea", "shortsea", "train", "truck" ],
        "description" : "Type of modality."
      }
    }
  }
}